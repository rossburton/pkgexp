# PkgData Explorer, or `pkgexp`

pkgexp is a tool to visually explore the OpenEmbedded `pkgdata`, which is the
generated package-scope metadata. Specifically, it is designed to answer common
questions regarding what has been built:

- What recipes have been built?
- What packages did those packages build?
- What files are in a specific package?
- What other packages does this package depend on?
- What packages depend on this package?

## Installation

Until this package is on PyPi you'll have to build and install it yourself.  Pip works for this:

```
$ pip3 install --user .
```

pkgexp depends on BottlePy and Humanize.

## Invocation

Currently, pkgexp needs to be told where the `pkgdata` directory is. It will
then scan the pkgdata and tell you what URL to open in a browser:

```
$ pkgexp tmp/pkgdata/generic-arm64/
...
Listening on http://127.0.0.1:8080/
Hit Ctrl-C to quit.
```

## Caveats

The biggest caveat so far is that the pkgdata isn't monitored for changes, so if
you do another build then pkgexp has to be quit (with `Control-C`) and
restarted.

The GitLab project page has a list of known issues and enhancements needed, and
merge requests are always welcome.

## License

Picobuild is MIT licensed, see `LICENSE` for terms.

pkgexp is Copyright (C) 2022 Arm Limited. It was written by Ross Burton
`<ross.burton@arm.com>`
