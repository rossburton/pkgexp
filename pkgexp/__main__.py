#! /usr/bin/env python3

import argparse
import pathlib
import sys

import pkgexp


def main(args=None) -> int:
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("pkgdata", type=pathlib.Path, help="Path to pkgdata")
    parser.add_argument(
        "--address", "-a", default="127.0.0.1", help="Address to bind to"
    )
    parser.add_argument("--port", "-p", type=int, default=8080, help="Port to use")
    args = parser.parse_args()

    pkgexp.harvest_pkgdata(args.pkgdata)
    pkgexp.webapp.run(host=args.address, port=args.port, debug=True)


if __name__ == "__main__":
    sys.exit(main())
