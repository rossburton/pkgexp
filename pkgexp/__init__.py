#! /usr/bin/env python3

"""
pkgdata browser.
"""

from __future__ import annotations

__version__ = "0.1"

import json
import pathlib
import dataclasses

import bottle


def explode_dep_versions2(s, *, sort=True):
    """
    Take an RDEPENDS style string of format:
    "DEPEND1 (optional version) DEPEND2 (optional version) ..."
    and return a dictionary of dependencies and versions.
    """
    import collections

    r = collections.OrderedDict()
    l = s.replace(",", "").split()
    lastdep = None
    lastcmp = ""
    lastver = ""
    incmp = False
    inversion = False
    for i in l:
        if i[0] == "(":
            incmp = True
            i = i[1:].strip()
            if not i:
                continue

        if incmp:
            incmp = False
            inversion = True
            # This list is based on behavior and supported comparisons from deb, opkg and rpm.
            #
            # Even though =<, <<, ==, !=, =>, and >> may not be supported,
            # we list each possibly valid item.
            # The build system is responsible for validation of what it supports.
            if i.startswith(("<=", "=<", "<<", "==", "!=", ">=", "=>", ">>")):
                lastcmp = i[0:2]
                i = i[2:]
            elif i.startswith(("<", ">", "=")):
                lastcmp = i[0:1]
                i = i[1:]
            else:
                # This is an unsupported case!
                raise Exception(
                    'Invalid version specification in "(%s" - invalid or missing operator'
                    % i
                )
                lastcmp = i or ""
                i = ""
            i.strip()
            if not i:
                continue

        if inversion:
            if i.endswith(")"):
                i = i[:-1] or ""
                inversion = False
                if lastver and i:
                    lastver += " "
            if i:
                lastver += i
                if lastdep not in r:
                    r[lastdep] = []
                r[lastdep].append(lastcmp + " " + lastver)
            continue

        # if not inversion:
        lastdep = i
        lastver = ""
        lastcmp = ""
        if not (i in r and r[i]):
            r[lastdep] = []

    if sort:
        r = collections.OrderedDict(sorted(r.items(), key=lambda x: x[0]))
    return r


def parse_pkgdata(filename, package=None):
    d = {}
    with open(filename) as data:
        for line in data:
            key, value = line.split(": ", 1)
            if package:
                key = key.replace(f":{package}", "")
            d[key] = value.strip()
    return d


recipes = {}
packages = {}
provides = {}


def mapped(**args):
    return dict(mapped=True, **args)


def unescape(s):
    return s.replace(r"\n", "\n").replace(r"\t", "\t")


@dataclasses.dataclass
class Package:
    package: str
    # Note that PN is the *recipe name* not the package name
    PN: str = dataclasses.field(metadata=mapped(), default=None)
    PE: str = dataclasses.field(metadata=mapped(), default="")
    PV: str = dataclasses.field(metadata=mapped(), default="")
    PR: str = dataclasses.field(metadata=mapped(), default="")
    PKG: str = dataclasses.field(metadata=mapped(), default=None)
    PKGE: str = dataclasses.field(metadata=mapped(), default="")
    PKGV: str = dataclasses.field(metadata=mapped(), default="")
    PKGR: str = dataclasses.field(metadata=mapped(), default="")
    PKGSIZE: int = dataclasses.field(metadata=mapped(transform=int), default=0)
    SUMMARY: str = dataclasses.field(metadata=mapped(), default=None)
    DESCRIPTION: str = dataclasses.field(metadata=mapped(), default=None)
    LICENSE: str = dataclasses.field(metadata=mapped(), default=None)
    ALLOW_EMPTY: int = dataclasses.field(metadata=mapped(transform=int), default=0)
    FILES: list[str] = dataclasses.field(
        metadata=mapped(transform=lambda s: s.split()), default_factory=list
    )
    FILES_INFO: dict[str, int] = dataclasses.field(
        metadata=mapped(transform=lambda s: json.loads(s)), default_factory=dict
    )
    RDEPENDS: dict = dataclasses.field(
        metadata=mapped(transform=explode_dep_versions2), default_factory=dict
    )
    RRECOMMENDS: dict = dataclasses.field(
        metadata=mapped(transform=explode_dep_versions2), default_factory=dict
    )
    RSUGGESTS: dict = dataclasses.field(
        metadata=mapped(transform=explode_dep_versions2), default_factory=dict
    )
    RPROVIDES: dict = dataclasses.field(
        metadata=mapped(transform=explode_dep_versions2), default_factory=dict
    )
    pkg_postinst: str = dataclasses.field(
        metadata=mapped(transform=unescape), default=None
    )
    pkg_postrm: str = dataclasses.field(
        metadata=mapped(transform=unescape), default=None
    )

    @staticmethod
    def parse(filename, name):
        data = parse_pkgdata(filename, name)
        package = Package(name)

        for field in dataclasses.fields(package):
            if not field.metadata.get("mapped", False):
                continue

            key = field.name
            if key not in data:
                continue

            value = data[key]
            transform = field.metadata.get("transform", None)
            if transform:
                value = transform(value)

            setattr(package, field.name, value)

        for provide in package.RPROVIDES:
            provides[provide] = name

        return package


@dataclasses.dataclass
class Recipe:
    name: str
    packages: dict[str, Package] = dataclasses.field(default_factory=dict)
    summary: str = None

    @staticmethod
    def parse(pkgdatadir, recipename):
        filename = pkgdatadir / recipename
        recipe = Recipe(recipename)

        data = parse_pkgdata(filename)
        for name in data["PACKAGES"].split():
            package_file = pkgdatadir / "runtime" / name
            package = Package.parse(package_file, name)
            recipe.packages[name] = packages[name] = package

        if recipename in recipe.packages:
            recipe.summary = recipe.packages[recipename].SUMMARY

        return recipe


def harvest_pkgdata(path):
    for entry in path.iterdir():
        if not entry.is_file():
            continue

        r = Recipe.parse(path, entry.name)
        recipes[r.name] = r


webapp = bottle.Bottle()
bottle.TEMPLATE_PATH = [str(pathlib.Path(__file__).with_name("views"))]


@webapp.route("/")
@webapp.route("/recipes")
@bottle.view("recipes")
def list_recipes():
    return dict(recipes=recipes)


@webapp.route("/recipes/<recipe>")
@bottle.view("recipe")
def recipe(recipe):
    return dict(recipe=recipe, packages=recipes[recipe].packages)
    # TODO nice not found page


@webapp.route("/packages/<package>")
def package(package):
    if package in packages:
        return bottle.template("package", **vars(packages[package]))
    elif package in provides:
        return bottle.redirect(f"/packages/{provides[package]}")
    # TODO nice not found page


@webapp.route("/depends/<name>")
@bottle.view("depends")
def depends(name):
    package = packages[name]
    # Expand this name to everything it provides
    names = set()
    names.add(name)
    names.update(package.RPROVIDES)

    package_list = []
    for package in packages.values():
        for relationship in "RDEPENDS", "RRECOMMENDS", "RSUGGESTS":
            if names & getattr(package, relationship).keys():
                package_list.append((relationship, package))
    return dict(name=name, packages=package_list)
    # TODO nice not found page
